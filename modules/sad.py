#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A simple sample module with Skype smileys
"""
import os
name = os.environ["SKYPE_FULLNAME"]
if not name:
  name = os.environ["SKYPE_USERNAME"]
print "No need to be sad %s (hug)" % name
