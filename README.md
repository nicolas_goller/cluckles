# Cluckles Welcomes You#
-------------
![skype.JPG](https://bitbucket.org/repo/RRrrbG/images/1160913071-skype.JPG)



Cluckles is a fork of [Sevabot](https://github.com/opensourcehacker/sevabot).


## Details ##
Current custom commands include:

* **?**
returns either true or false

* **define word**
returns a definition from dictionary.com

* **feel**
returns an emoticon relating to an emotion

* **from**
returns a random country

* **song**
a playlist manager

* **h**
let's you check people's statuses on houroma

* **math**
parses math expressions and returns a value using python's parser. Also can multiply and add en masse.

* **g (for google)**
queries google for the text you enter and returns the first three links. Some links aren't working properly (or are obscenely long) at the moment.

* **feedback**
writes a feedback file.

* **and more**
don't worry, that's not all...