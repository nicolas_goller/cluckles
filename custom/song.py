#!/usr/bin/env python
import random
import sys
import os

path = os.path.join(os.path.expanduser('~'), 'sevabot','custom','resources','playlist')
f = open(path, 'r')
lines = list(f);

def grabRandom():
  if len(lines) > 0:
    print lines[random.randrange(len(lines))]
  else:
    print "Nothing on the list :(, add something with !song add."

def add(str):
  str += '\n'
  if str in lines:
    print "Already added..."
  else:
    lines.append(str)
    write(f)
    print 'Added!'

def write(f):
  f.close()
  f = open(path, 'w')
  for line in lines:
    f.write(line)

def remove(strIndex):
  try:
    index = int(strIndex)
    del lines[index]
    print "Removed."
    write(f)
  except:
    print "Something went wrong"

def grabIndex(s):
  try:
    index = int(s)

    if index < len(lines) and index > -len(lines) - 1:
      print lines[index]
    else:
      print "Valid indices: Anything less than " + str(len(lines)) + " and greater than - " + str(len(lines)+1) + "."
  except IndexError:
    print "Cluckles confused...what you want?"
  except:
    print "oops"

def range(begin,end):
  try:
    indexStart = int(begin)
    indexEnd = int(end)
    while indexStart < indexEnd and indexStart < len(lines):
      print str(indexStart) + ': ' + lines[indexStart]
      indexStart += 1
  except:
    print "All sorts of things went wrong"


if len(sys.argv) < 2:
  grabRandom()
elif sys.argv[1].lower() == 'help':
    print '(music) Playlist managing command.\n' + \
    '!song              grabs a random song.\n' + \
    '!song add link     adds a song.\n' + \
    '!song index        grabs song at index.\n' + \
    '!song list         lists songs.\n' +\
    '!song remove       removes songs.\n'
elif sys.argv[1].lower() == 'add':
  add(" ".join(sys.argv[2:]))
elif sys.argv[1].lower() == 'count':
  print len(lines)
elif len(sys.argv) >= 3 and sys.argv[1].lower() == 'remove':
  remove(sys.argv[2])
elif sys.argv[1].lower() == 'list':
  if len(sys.argv) == 2:
    range(0,5)
  elif len(sys.argv) == 3:
    range(0,sys.argv[2])
  elif (sys.argv >= 4):
    range(sys.argv[2],sys.argv[3])
else:
  grabIndex(sys.argv[1])

f.close()