#!/usr/bin/env python
import sys
import re
import urllib2
import bs4


def find(x):
    query=str(x)
    total = 0
    
    try:
        req = urllib2.Request("https://www.google.com/search?q="+urllib2.quote(query), headers={'User-Agent':"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:41.0) Gecko/20100101 Firefox/41.0"})
        con = urllib2.urlopen(req)
        page = con.read()
        soup = bs4.BeautifulSoup(page, "html.parser")

        while total < 3:
            out = soup.select('h3 a[href]')[total]['href']
            if '/search?q=' in out:
                print "Google Images Result"
            else:
                print soup.select('h3 a[href]')[total].contents[0]
                print out.replace('/url?q=','')
            total += 1

    except IndentationError:
        print 'indent'

if len(sys.argv) < 2:
    'Gimme something to google please'
elif sys.argv[1].lower() == 'help':
    print 'Cluck. G stands for Google.\nCluckles will google something for you and provide links!\n'
elif 'cluckles' in sys.argv[1].lower():
    print 'Cluck cluck...cluck!'
else:
    find((" ".join(sys.argv[1:])).strip())