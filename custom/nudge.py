#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A simple sample module with Skype smileys
"""
import os
import time
import sys

if len(sys.argv) > 1:
  print "Hello " + sys.argv[1] + "."
else:
  print "Cluck cluck...huah huah!"