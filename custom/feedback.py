#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A simple sample module with Skype smileys
"""
import os
import sys
import random

path = os.path.join(os.path.expanduser('~'), 'sevabot','custom','resources','feedback')
try:
  f = open(path, 'a')
  f.write(" ".join(sys.argv[1:]) + "\n")
  f.close()  
  chance = random.random()
  if chance < .5:
    print "It's on the list now, thanks." 
  elif chance < .7:
    print "Cluck...I'll keep it in mind"
  elif chance < .8:
    print "That's an odd suggestion... you (drunk)?"
  elif chance < .95:
    print "Cluckles Rising (celebrate)"
  elif chance < .99:
    print "Not gonna happen pal (shake)"
  elif chance >= .99:
    print "You are one in a thousand...literally...(cat)"
  else:
    print "Cluckles sleepy"
except:
  print "Cluckles got confused"
