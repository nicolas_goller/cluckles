#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A simple sample module with Skype smileys
"""
import os
import random

emotes = [
  '(cool)', ':)', ':(', ":'(", '(sweat)',
  ':|', ':^)', '(snooze)', '(dull)',
  '(yawn)', '(angry)', '(party)', ':S',
  '(happy)', '(smirk)', '(tmi)', '(rock)',
  '(punch)', '(swear)', '(rock)', '(talktothehand)'
]

print emotes[int(random.random() * len(emotes))]