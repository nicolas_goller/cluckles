#!/usr/bin/env python
import sys
import urllib2
import re

def find(x):
    srch=str(x)
    try:
        x = urllib2.urlopen("http://dictionary.reference.com/browse/"+srch+"?s=t")

        x = x.read()
        items = re.findall('<meta name="description" content="'+".*$",x,re.MULTILINE)
        for x in items:
            y =x.replace('<meta name="description" content="','')
            m =re.findall('at Dictionary.com, a free online dictionary with pronunciation,              synonyms and translation. Look it up now! "/>',y)
            g =re.findall('pageName="dict-home"',y)
            if m == [] and g == []:
                if y.startswith("Wcs definition"):
                    print "Word not found! :("
                    return
                else:
                    z = y.replace(' definition,',":")                
                    trunc = z.find(''' See more."/>''')
                    print z[0:trunc]
                    return
        print "Cluckles thinks that word is nonsense!"
    except:
        print "What is word?! Cluckles is sorry."
        return                

if len(sys.argv) < 2:
    print "You must give cluckles something to look up..."
elif 'cluckles' in sys.argv[1].lower():
    print 'Come on, you should know me better than that...'
else:
    find(sys.argv[1])