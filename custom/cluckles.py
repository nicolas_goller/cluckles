#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A simple sample module with Skype smileys
"""
import os
name = os.environ["SKYPE_FULLNAME"]
if not name:
  name = os.environ["SKYPE_USERNAME"]
print "I'm cluckles. I'm a chicken. (happy). How are you %s?" % name
