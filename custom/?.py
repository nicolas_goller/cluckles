#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A simple sample module with Skype smileys
"""
import random as rand

randomValue = rand.random()

if randomValue > .5:
  print "No   (shake)"
elif randomValue < .49:
  print "Yes  (nod)"
elif randomValue < .495:
  print "I dunno?! :$"
elif randomValue < .499:
  print "This is an important question that you must find the answer to yourself.\nGo now, young one."
else:
  print "Wow...golden. I shall frame this remark."
