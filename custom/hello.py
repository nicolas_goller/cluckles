#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A simple sample module with Skype smileys
"""
import os

print "Hi %s (wave)" % os.environ["SKYPE_FULLNAME"]
