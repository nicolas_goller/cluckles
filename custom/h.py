#!/usr/bin/env python
import sys
import urllib2
import re
import random

def find(x):
    username=str(x)
    try:
        x = urllib2.urlopen("https://www.houroma.com/"+username)

        x = x.read()
        items = re.findall('<span style="color:rgb\(50,180,50\)">(.*?)</span>',x,re.DOTALL)
        
        if items:
            print username.capitalize() + " has been " + items[0].strip() + " for " + items[1].strip()
        else:
            items = re.findall('<span style="color:rgb\(180,180,50\)">(.*?)</span>',x,re.DOTALL)
            
            if items:
                print username.capitalize() + " is not active"
            else:
                print "Cluck cluck, nothing doing."

    except:
        print "This username makes website cry. Please pick nice name :(."
        return                

if len(sys.argv) < 2:
    'Gimme a username please'
elif sys.argv[1].lower() == 'help':
    print 'Cluck. H stands for https://www.houroma.com\nCluckles can give you info!\n!h for random user\n!h username for specific user.\n'
elif 'cluckles' in sys.argv[1].lower():
    print 'Cluck cluck...cluck!'
else:
    find(sys.argv[1])