#!/usr/bin/env python
import sys
from math import *
import parser
try:
  if len(sys.argv) < 2 or sys.argv[1].lower() == 'help':
    print "Possible commands are exp, log, log10, pow, sqrt, pi, e. Or just type a mathematical expression and cluckles will evaluate."
  elif sys.argv[1] == '*' or sys.argv[1].lower() == 'mul':
    print reduce(lambda x, y: float(x)*float(y), sys.argv[2:])
  elif sys.argv[1] == '+' or sys.argv[1].lower() == 'add':
    print reduce(lambda x, y: float(x)+float(y), sys.argv[2:])    
  elif sys.argv[1].lower() == 'exp' or sys.argv[1] == '^' or sys.argv[1] == '**':
    print exp(float(sys.argv[2]))
  elif sys.argv[1].lower() == 'log':
    print log(float(sys.argv[2]))
  elif sys.argv[1].lower() == 'log10':
    print log10(float(sys.argv[2]))
  elif sys.argv[1].lower() == 'pow':
    print log10(float(sys.argv[2]),float(sys.argv[3]))
  elif sys.argv[1].lower() == 'sqrt':
    print sqrt(float(sys.argv[2]))
  elif sys.argv[1].lower() == 'pi':
    print pi
  elif sys.argv[1].lower() == 'e':
    print e
  else:
    code = parser.expr(sys.argv[1]).compile()
    print eval(code)
except:
  print 'Cluckles confused.'